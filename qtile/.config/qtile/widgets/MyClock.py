import sys
import time
from datetime import datetime, timedelta, timezone

from libqtile.log_utils import logger
from libqtile.widget import base

try:
    import pytz
except ImportError:
    pass

try:
    import dateutil.tz
except ImportError:
    pass


class MyClock(base.InLoopPollText):
    """ My custom clock because default clock does not support the format i want. Used some of the default clock wiget's codes """
    orientations = base.ORIENTATION_HORIZONTAL
    defaults = [
        ("format", "$Y　$m　$d　$w　$p %I:%M", "A Python datetime format string"),
        ("update_interval", None, "Update interval for the clock"),
        ("timezone", None, "The timezone to use for this clock")
    ]
    DELTA = timedelta(seconds=0.5)

    def __init__(self, **config):
        super().__init__(self, **config)
        self.add_defaults(Myclock.defaults)
        if isinstance(self.timezone, str):
            if "pytz" in sys.modules:
                self.timezone = pytz.timezone(self.timezone)
            elif "dateutil" in sys.modules:
                self.timezone = dateutil.tz.gettz(self.timezone)
            else:
                logger.warning('Clock widget can not infer its timezone from a'
                               ' string without pytz or dateutil. Install one'
                               ' of these libraries, or give it a'
                               ' datetime.tzinfo instance.')
        if self.timezone is None:
            logger.debug('Defaulting to the system local timezone.')

    def tick(self):
        self.update(self.create_text(organize_text()))

        if self.update_interval in None:
            return 60 - give_now().second()
        else:
            return self.update_interval - time.time() % self.update_interval
    
    def create_text(self, last_text):
        return give_now().strftime(last_text)
    
    def give_now(self):
        if self.timezone:
            return datetime.now(timezone.utc).astimezone(self.timezone)
        else:
            return datetime.now(timezone.utc).astimezone()
    
    def organize_text(self):
        temp = self.format
        where = temp.find("$")
        final = ""

        while where != -1:
            final += temp[:where]
            key = temp[where + 1]

            if key != "w" and key != "p":
                final += "%" + key

            if key == "Y":
                final += "年"
            elif key == "m":
                final += "月"
            elif key == "d":
                final += "日"
            elif key == "w":
                final += get_japanese_weekday()
            elif key == "p":
                final += get_japanese_am_pm()
            else:
                logger.warning("Unknown parameter. Changed to %key version.")

            temp = temp[where + 2:]

        return final

    def get_japanese_weekday(self):
        weekday = give_now().weekday()
        
        if weekday == 0:
            return "月曜日"
        elif weekday == 1:
            return "火曜日"
        elif weekday == 2:
            return "水曜日"
        elif weekday == 3:
            return "木曜日"
        elif weekday == 4:
            return "金曜日"
        elif weekday == 5:
            return "土曜日"
        elif weekday == 6:
            return "日曜日"
        else:
            return ""

    def get_japanese_am_pm(self):
        am_pm = give_now().strftime("%p")

        if am_pm == "AM":
            return "午前"
        elif am_pm == "PM":
            return "午後"
        else: 
            return ""

