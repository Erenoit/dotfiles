# vim:fileencoding=utf-8:foldmethod=marker

# IMPORTS {{{
from os               import path
from datetime         import datetime
import subprocess

from libqtile         import qtile, layout, bar, widget, hook
from libqtile.config  import Screen, Group, ScratchPad, DropDown, Key, Click, Drag
from libqtile.lazy    import lazy
# }}}

# INIT {{{

mod = "mod4"
alt = "mod1"
home = path.expanduser('~')
keys = []
# }}}

# DEFAULT PROGRAMS {{{

my_terminal    = "alacritty"
my_browser     = "firefox"
my_filemanager = "pcmanfm"
my_todo        = "superproductivity"
# }}}

# FONT {{{

font = "Iosevka Nerd Font"
font_size = 10
# }}}

# COLORS {{{

# HackTheBox theme
# https://github.com/audibleblink/hackthebox.vim
hackthebox = {
    "background":   "#1a2332",
    "foreground":   "#a4b1cd",

    "background_l": "#313f55",
    "foreground_l": "#ffffff",

    "black":        "#000000",
    "red":          "#ff3e3e",
    "green":        "#9fef00",
    "yellow":       "#ffaf00",
    "blue":         "#004cff",
    "magenta":      "#9f00ff",
    "cyan":         "#2ee7b6",
    "white":        "#ffffff",

    "black_l":      "#666666",
    "red_l":        "#ff8484",
    "green_l":      "#c5f467",
    "yellow_l":     "#ffcc5c",
    "blue_l":       "#5cb2ff",
    "magenta_l":    "#c16cfa",
    "cyan_l":       "#5cecc6",
    "white_l":      "#ffffff",
}

# Tokyo Night Theme
# https://github.com/folke/tokyonight.nvim
tokyonight = {
    "background":   "#1a1b26",
    "foreground":   "#c0caf5",

    "background_l": "#30313b",
    "foreground_l": "#c6cff6",

    "black":        "#15161E",
    "red":          "#f7768e",
    "green":        "#9ece6a",
    "yellow":       "#e0af68",
    "blue":         "#004cff", #from hackthebox theme
    "magenta":      "#bb9af7",
    "cyan":         "#7dcfff",
    "white":        "#a9b1d6",

    "black_l":      "#414868",
    "red_l":        "#f78399",
    "green_l":      "#a7d278",
    "yellow_l":     "#e3b777",
    "blue_l":       "#87abf7",
    "magenta_l":    "#c1a4f7",
    "cyan_l":       "#8ad3ff",
    "white_l":      "#c0caf5",
}

colors = tokyonight
# }}}

# GROUPS {{{

groups = [
        Group(name="1", label=" ", layout="monadtall"),
        Group(name="2", label=" ", layout="max"),
        Group(name="3", label=" ", layout="floating"),
        Group(name="4", label="ﭮ ", layout="max"),
        Group(name="5", label=" ", layout="max"),
        Group(name="6", label=" ", layout="monadtall"),
        Group(name="7", label=" ", layout="max"),
        Group(name="8", label=" ", layout="floating"),
        Group(name="9", label=" ", layout="max"),
        Group(name="0", label=" ", layout="floating"),
        ]
for group in groups:
    keys.extend([
        Key([mod], group.name, lazy.group[group.name].toscreen()),
        Key([mod, "shift"], group.name, lazy.window.togroup(group.name))
    ])
# }}}

# Scratchpads {{{

scratchpad_def = {
    "on_focus_lost_hide": True,
    "opacity"           : 1.00,
    "height"            : 0.60,
    "width"             : 0.60,
    "x"                 : 0.20,
    "y"                 : 0.20,
}

groups.append(
    ScratchPad("ScratchPad_group", [
        DropDown("ncmpcpp", my_terminal + " --class dropdown_term -e ncmpcpp", **scratchpad_def),
        DropDown("todo", my_todo, **scratchpad_def),
        DropDown("terminal", my_terminal + " --class dropdown_term", **scratchpad_def),
        DropDown("ranger", my_terminal + " --class dropdown_term -e ranger", **scratchpad_def),
        ]
    ))
# }}}

# LAYOUTS {{{

layout_theme = {
    "border_width":  2,
    "margin":        10,
    "border_focus":  colors["blue"],
    "border_normal": colors["blue_l"],
}

layouts = [
    layout.MonadTall(**layout_theme),     
    layout.Max(**layout_theme),
    layout.Stack(num_stacks=2),     
    layout.Floating(**layout_theme),
    layout.RatioTile(**layout_theme),
    layout.TreeTab(          
        font = font,          
        fontsize = font_size,          
        sections = ["FIRST", "SECOND", "THIRD", "FOURTH"],
        section_fontsize = 10,          
        border_width = layout_theme["border_width"],
        bg_color = colors["background"],          
        active_bg = colors["background_l"],
        active_fg = colors["foreground_l"],          
        inactive_bg = colors["black_l"],          
        inactive_fg = colors["black"],
        padding_left = 0,          
        padding_x = 0,
        padding_y = 5,          
        section_top = 10,
        section_bottom = 20,          
        level_shift = 8,
        vspace = 3,          
        panel_width = 200          ),
    #layout.MonadWide(**layout_theme),
    #layout.Bsp(**layout_theme),     
    #layout.Stack(stacks=2,**layout_theme),     
    #layout.Columns(**layout_theme),
    #layout.RatioTile(**layout_theme),
    #layout.Tile(shift_windows=True, **layout_theme),
    #layout.VerticalTile(**layout_theme),
    #layout.Matrix(**layout_theme),     
    #layout.Zoomy(**layout_theme),
]

loating_layout = layout.Floating(float_rules=[
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},
    {'wmclass': 'makebranch'},
    {'wmclass': 'maketag'},
    {'wmclass': 'Arandr'},
    {'wmclass': 'feh'},
    {'wmclass': 'Galculator'},
    {'wname':   'branchdialog'},
    {'wname':   'Open File'},
    {'wname':   'pinentry'},
    {'wmclass': 'ssh-askpass'},
    {'wmclass': 'lxpolkit'},
    {'wmclass': 'Lxpolkit'},
    {'wmclass': 'yad'},
    {'wmclass': 'Yad'},


],  fullscreen_border_width = 0, border_width = 0)
auto_fullscreen = True

focus_on_window_activation = "smart" # smart or focus
# }}}

# KEYS {{{

keys.extend([
    #### Scratchpads
    Key([mod], "t",
        lazy.group["ScratchPad_group"].dropdown_toggle("todo"),
        desc = "Opens prefered todo list app on scratchpad"
        ),
    Key([mod], "x",
        lazy.group["ScratchPad_group"].dropdown_toggle("ncmpcpp"),
        desc = "Opens ncmpcpp on scratchpad"
        ),
    Key([mod], "c",
        lazy.group["ScratchPad_group"].dropdown_toggle("terminal"),
        desc = "Opens prefered terminal on scratchpad"
        ),
    Key([mod], "z",
        lazy.group["ScratchPad_group"].dropdown_toggle("ranger"),
        desc = "Opens ranger on scratchpad"
        ),

    #### Window Control
    Key([mod, "shift"],  "r",        
        lazy.restart(),
        desc = "Restarts the Qtile"
        ),
    Key([mod, "shift"],  "q",        
        lazy.shutdown(),
        desc = "Closes The Qtile"
        ),
    Key([mod],  "q",        
        lazy.window.kill(),
        desc = "Closes the focused window"
        ),
    Key([mod],  "f",        
        lazy.window.toggle_fullscreen(),
        desc = "Makes the focused window fullscreen"
        ),
    Key([mod],  "d",        
        lazy.window.toddle_floating(),
        desc = "Puts the focused window to floating mode"),
  
    # Layout Shortcuts {{{
    
    # MonadTall
#    Key([mod],  "h",        
#        lazy.layout.grow_main(),
#        desc = ""),
#    Key([mod],  "j",        
#        lazy.spawn(""),
#        desc = ""),
#    Key([mod],  "k",        
#        lazy.spawn(""),
#        desc = ""),
#    Key([mod],  "l",        
#        lazy.layout.shrink_main(),
#        desc = ""),
#    Key([mod],  "Left",        
#        lazy.spawn(""),
#        desc = ""),
#    Key([mod],  "Down",        
#        lazy.spawn(""),
#        desc = ""),
#    Key([mod],  "Up",        
#        lazy.spawn(""),
#        desc = ""),
#    Key([mod],  "Right",        
#        lazy.spawn(""),
#        desc = ""),
#    Key([mod],  "*",        
#        lazy.spawn(""),
#        desc = ""),
    # }}}
])
# }}}

# WIDGET FUNCTIONS {{{

def get_time_format():
    format_callendar  = "%Y年　%m月　%d日"
    format_clock_base = "%I:%M"

    weekday = datetime.today().weekday()
    if weekday == 0:
        format_weekday = "月曜日"
    elif weekday == 1:
        format_weekday = "火曜日"
    elif weekday == 2:
        format_weekday = "水曜日"
    elif weekday == 3:
        format_weekday = "木曜日"
    elif weekday == 4:
        format_weekday = "金曜日"
    elif weekday == 5:
        format_weekday = "土曜日"
    elif weekday == 6:
        format_weekday = "日曜日"

    am_pm = datetime.today().strftime("%p")
    if am_pm == "AM":
        format_am_pm = "午前"
    elif am_pm == "PM":
        format_am_pm = "午後"

    final_format = format_callendar + "　" + format_weekday + "　" + format_am_pm + " " + format_clock_base
    return final_format
# }}}

# WIDGETS {{{

widgets_list = [
    widget.Sep(
        linewidth  = 0,
        padding    = 6,
        background = colors["background"],
        foreground = colors["foreground"]
    ),
    widget.GroupBox(
        font                       = font,
        fontsize                   = font_size,
        margin_x                   = 0,
        margin_y                   = 0,
        padding_x                  = 5,
        padding_y                  = 5,
        borderwidth                = 1,
        background                 = colors["background"],
        foreground                 = colors["foreground"],
        highlight_method           = "line",
        highlight_color            = colors["red"],
        rounded                    = False,
        this_current_screen_border = colors["background_l"],
        use_mouse_wheel            = True,
    ),
    widget.TaskList(
        font             = font,
        fontsize         = font_size,
        background       = colors["background"],
        foreground       = colors["foreground"],
        margin           = 2,
        borderwidth      = 1,
        border           = colors["blue_l"],
        unfocused_border = colors["blue"],
        rounded          = False,
        icon_size        = 10,
        max_title_width  = 150,
        txt_minimized    = "(MIN)",
        txt_floating     = "🗗 ",
    ),
    widget.Systray(
        font = font,
        fontsize = font_size,
        background = colors["background"],
    ),                                   
    widget.CurrentLayoutIcon(            
        font = font,                     
        fontsize = font_size,            
        background = colors["background"],
        foreground = colors["blue"],
    ),                                   
    widget.CurrentLayout(                
        font = font,                     
        fontsize = font_size,            
        background = colors["background"],
        foreground = colors["blue"],
    ),                                   
    widget.TextBox(                      
        font       = font,                     
        fontsize   = font_size,            
        background = colors["background"],
        foreground = colors["yellow"],
        text       = ""
                                         
    ),                                   
    widget.CheckUpdates(                 
        font                = font,                     
        fontsize            = font_size,            
        background          = colors["background"],
        foreground          = colors["yellow"],
        colour_have_updates = colors["yellow"],
        colour_no_updates   = colors["yellow"],
        display_format      = "ﰭ {updates}",
        no_update_string    = "No Update",
    ),                                   
    #CriptoTicker                        
    widget.TextBox(                      
        font       = font,                     
        fontsize   = font_size,            
        background = colors["background"],
        foreground = colors["red"],
        text       = ""
    ),                                   
    widget.ThermalSensor(                
        font       = font,                     
        fontsize   = font_size,            
        background = colors["background"],
        foreground = colors["red"],
        metric     = True,                                 
    ),                                   
    widget.TextBox(                      
        font       = font,                     
        fontsize   = font_size,            
        background = colors["background"],
        foreground = colors["cyan"],
        text       = ""
    ),                                   
    widget.CPU(                          
        font       = font,                     
        fontsize   = font_size,            
        background = colors["background"],
        foreground = colors["cyan"],
        format     = " {freq_current}GHz {load_percent}%",
    ),                                   
    widget.TextBox(                      
        font       = font,                     
        fontsize   = font_size,            
        background = colors["background"],
        foreground = colors["green"],
        text       = ""
    ),                                   
    widget.Memory(                      
        font       = font,                     
        fontsize   = font_size,            
        background = colors["background"],
        foreground = colors["green"],
        format     = "{MemUsed: .0f}{mm}/{MemTotal: .0f}{mm}"
    ),                                   
    widget.TextBox(                      
        font       = font,                     
        fontsize   = font_size,            
        background = colors["background"],
        foreground = colors["magenta"],
        text       = ""
    ),                                   
    widget.Volume(                  
        font                = font,                     
        fontsize            = font_size,            
        background          = colors["background"],
        foreground          = colors["magenta"],
        #theme_path          = home + "/.config/qtile/icons/volume_icons",
        #limit_max_volume    = True,
        #get_volume_command  = "pulsemixer --get-volume",
        #mute_command        = "pulsemixer --toggle-mute",
        #volume_up_command   = "pulsemixer --change-volume +1",
        #volume_down_command = "pulsemixer --change-volume -1",
        #volume_app          = "pulsemixer",
    ),                                   
    #widget.BatteryIcon(                  
    #    font       = font,                     
    #    fontsize   = font_size,            
    #    background = colors["background"],
    #    foreground = colors["blue"],
    #    theme_path = home + "/.config/qtile/icons/battery_icons",             
    #),                                   
    widget.Battery(                      
        font         = font,                     
        fontsize     = font_size,            
        background   = colors["background"],
        foreground   = colors["blue"],
        notify_below = 20,
        format       = " {percent:2.0%}",
    ),
    widget.Clock(
        font       = font,
        fontsize   = font_size,
        background = colors["background"],
        foreground = colors["yellow"],
        timezone   = "Europe/Istanbul",
        format     = get_time_format()
    )
]
# }}}

# SCREENS {{{

screens = [
    Screen(
        top=bar.Bar(widgets=widgets_list, opacity=1.0, size = 20,
        background="000000")
    ),
    Screen(
        top=bar.Bar(widgets=widgets_list, opacity=1.0, size = 20,
        background="000000")
    ),
    Screen(
        top=bar.Bar(widgets=widgets_list, opacity=1.0, size = 20,
        background="000000")
    ),
]
# }}}

# MOUSE {{{

mouse = [
    Drag([mod], "Button1", 
         lazy.window.set_position_floating(),
         start=lazy.window.get_position()
         ),
    Drag([mod], "Button3", 
         lazy.window.set_size_floating(),
         start=lazy.window.get_size()
         ),
]

follow_mouse_focus = True
bring_front_click  = False
cursor_wrap        = False
# }}}

# HOOKS {{{

@hook.subscribe.client_new
def assign_app_group(client):
    d = {}
    
    ##### Assign Apps to Groups #####
    d["1"] = ["Alacritty", "Kitty", "Termite", "St", 
              "alacritty", "kitty", "termite", "st"]
    d["2"] = ["Navigator", "Firefox", "Vivaldi-stable", "Vivaldi-snapshot", "Chromium", "Google-chrome", "Brave", "Brave-browser",
              "navigator", "firefox", "vivaldi-stable", "vivaldi-snapshot", "chromium", "google-chrome", "brave", "brave-browser", ]
    d["3"] = ["Steam", "Lutris", "Wine", "Proton", 
              "steam", "lutris", "wine", "proton"]
    d["4"] = ["Discord", "TelegramDesktop", 
              "discord", "telegramDesktop"]
    d["5"] = ["Spotify", "Pragha", "Clementine", "Deadbeef", "Audacious", "Vlc", "Mpv"
              "spotify", "pragha", "clementine", "deadbeef", "audacious", "vlc", "mpv" ]
    d["6"] = ["pcmanfm", "Nemo", "Caja", "Nautilus", "org.gnome.Nautilus", "Pcmanfm", "Pcmanfm-qt",
              "pcmanfm", "nemo", "caja", "nautilus", "org.gnome.nautilus", "pcmanfm", "pcmanfm-qt", ]
    d["7"] = ["Evolution", "Geary", "Mail", "Thunderbird", "Mailspring"
              "evolution", "geary", "mail", "thunderbird", "mailspring" ]
    d["8"] = ["Inkscape", "Nomacs", "Ristretto", "Nitrogen", "Feh", "Gimp",
              "inkscape", "nomacs", "ristretto", "nitrogen", "feh", "gimp" ]
    d["9"] = ["OBS", "Obs", "obs", "Audacity", "audacity"]
    d["0"] = ["VirtualBox Manager", "VirtualBox Machine", "Vmplayer",
              "virtualbox manager", "virtualbox machine", "vmplayer", ]
    
    ##############################
    wm_class = client.window.get_wm_class()[0]

    for i in range(len(d)):
        if wm_class in list(d.values())[i]:
            group = list(d.keys())[i]
            client.togroup(group)
            #client.group.cmd_toscreen()

@hook.subscribe.startup_once
def startup_once():
    subprocess.call([home + "/.scripts/autostart"])

@hook.subscribe.startup
def start_always():
    # Set the cursor to something sane in X
    subprocess.Popen(["xsetroot", "cursor_name", "left_ptr"])

@hook.subscribe.client_new
def set_floating(window):
    if (window.window.get_wm_transient_for()
            or window.window.get_wm_type() in floating_types):
        window.floating = True

floating_types = ["notification", "toolbar", "splash", "dialog"]
# }}}

# JAVA UI SUPPORT (wmname) {{{

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this 
# string besides java UI toolkits; you can see several discussions on the 
# mailing lists, GitHub issues, and other WM documentation that suggest setting 
# this string if your java app doesn't work correctly. We may as well just lie 
# and say that we're a working one by default. 
# 
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in 
# java that happens to be on java's whitelist. 
wmname = "LG3D"
# }}}

