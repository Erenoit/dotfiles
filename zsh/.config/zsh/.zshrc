# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.config/zsh/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

setopt extended_history       # record timestamp of command in HISTFILE
setopt hist_expire_dups_first # delete duplicates first when HISTFILE size exceeds HISTSIZE
setopt hist_ignore_dups       # ignore duplicated commands history list
setopt hist_ignore_space      # ignore commands that start with space
setopt hist_verify            # show command with history expansion to user before running it
setopt share_history          # share command history data

source "$ZDOTDIR/zsh-functions"

zsh_update

zsh_add_file "zsh-aliases"
zsh_add_file "zsh-completions"
zsh_add_file "zsh-keybindings"
#zsh_add_file "zsh-vim-mode"
#zsh_add_file "zsh-cursor"

zsh_add_plugin "zsh-users/zsh-syntax-highlighting"
zsh_add_plugin "zsh-users/zsh-autosuggestions"
zsh_add_plugin "zsh-users/zsh-completions"
zsh_add_plugin "zsh-users/zsh-history-substring-search"

zsh_add_plugin "hlissner/zsh-autopair"
zsh_add_plugin "oz/safe-paste"

zsh_add_plugin "Aloxaf/fzf-tab"
zsh_add_plugin "joshskidmore/zsh-fzf-history-search"

zsh_add_theme "romkatv/powerlevel10k"
[[ ! -f ~/.config/zsh/.p10k.zsh ]] || source ~/.config/zsh/.p10k.zsh

eval "$(zoxide init --cmd cd zsh)"
