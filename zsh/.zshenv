typeset -U PATH path

# XDG Paths
export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"} 

# Fcitx
export GTK_IM_MODULE="fcitx"
export QT_IM_MODULE="fcitx"
export SDL_IM_MODULE="fcitx"
export GLFW_IM_MODULE="ibus"
export XMODIFIERS="@im=fcitx"

# Fix JAVA
export _JAVA_AWT_WM_NONREPARENTING=1

# Timeshift autosnap
export maxSnapshots=2

# ZSH Paths
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"   # ZSH config directory

export HISTFILE="$ZDOTDIR/.zsh_history" # History filepath
export HISTSIZE=100000                  # Maximum events for internal history
export SAVEHIST=100000                  # Maximum events in history file
export HIST_STAMPS="yy-mm-dd"           # Time stamps style
export ZSH_FZF_HISTORY_SEARCH_BIND='^H'

# Default Apps
export EDITOR="nvim"
export READER="zathura"
export VISUAL="nvim"
export TERMINAL="st"
export BROWSER="firefox"
export VIDEO="mpv"
export IMAGE="feh"
export COLORTERM="truecolor"
export OPENER="xdg-open"
export PAGER="moar"
export WM="Hyprland"

# Colored Man Pages
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'

# fzf
export FZF_DEFAULT_COMMAND="rg --files --follow --no-ignore-vcs --hidden -g '!{**/node_modules/*,**/.git/*}'"
export FZF_DEFAULT_OPTS=$FZF_DEFAULT_OPTS' --color=fg:#a4b1cd,bg:-1,hl:#ffcc5c 
                                           --color=fg+:#a4b1cd,bg+:#1a2332,hl+:#ffcc5c 
                                           --color=info:#9fef00,prompt:#004cff,pointer:#9f00ff 
                                           --color=marker:#5cb2ff,spinner:#ff3e3e,header:#313f55'

# Scaling
export QT_AUTO_SCREEN_SCALE_FACTOR=0
export QT_SCALE_FACTOR=1
export QT_SCREEN_SCALE_FACTORS="1;1;1"
export GDK_SCALE=1
export GDK_DPI_SCALE=1

# Haskel
[ -f "/home/erenoit/.ghcup/env" ] && source "/home/erenoit/.ghcup/env" # ghcup-env

# PATH
export PATH="$PATH\
:/usr/local/bin\
:$HOME/.local/bin\
:$HOME/.scripts\
:$HOME/.scripts/menu\
:$HOME/.scripts/statusbar\
:$XDG_DATA_HOME/gem/ruby/3.0.0/bin\
:$HOME/.yarn/bin\
:$HOME/.cargo/bin\
:$HOME/go/bin\
:$HOME/.npm/bin\
:$HOME/.node/bin\
"

