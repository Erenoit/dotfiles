#! /bin/sh

SOCKET="$XDG_RUNTIME_DIR/hypr/$HYPRLAND_INSTANCE_SIGNATURE/.socket2.sock"

workspace_state() {
    WORKSPACE=$(printf "%s" $2 | awk '{split($0, a, ">>"); print a[2]}')

    STATE_STR=$(eww state \
        | awk '/is_empty/ {split($0, a, ": "); print a[2]}' \
        | sed -e "s/\[//g" -e "s/\]//g" -e "s/,//g")
    STATE=( $STATE_STR )

    case $1 in
        create)  STATE[$WORKSPACE - 1]=false ;;
        destroy) STATE[$WORKSPACE - 1]=true  ;;
    esac

    eww update is_empty="$(printf "[%s]" "${STATE[*]}" | sed -e "s/ /, /g")"
}

workspace_window_count() {
    eww update active_info="[$(hyprctl activeworkspace | awk '/ *windows:/ {print $2}')]"
}

workspace_active() {
    eww update active_workspace=$(printf "%s" $1 | awk '{split($0, a, ">>"); print a[2]}')
}

window_active() {
    eww update active_window="$(printf "%s" "$1" | awk '{split($0, a, ","); print a[2]}')"
}

handle() {
    case $1 in
        workspace*) workspace_active "$1";  workspace_window_count ;;
        createworkspace*)  workspace_state create  "$1" ;;
        destroyworkspace*) workspace_state destroy "$1" ;;
        openwindow*) workspace_window_count ;;
        closewindow*) window_active ", "; workspace_window_count ;;
        "activewindow>>"*) window_active "$1" ;;
    esac
}

socat -U - UNIX-CONNECT:$SOCKET | \
    while read -r line; do
        handle "$line"
    done
