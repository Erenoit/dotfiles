# vim:fileencoding=utf-8:foldmethod=marker

# DIRECTORIES {{{

ncmpcpp_directory = ~/.config/ncmpcpp
lyrics_directory = ~/Music/.lyrics
mpd_music_dir = ~/Music
# }}}

# CONNECTION {{{

mpd_host = localhost
mpd_port = 6600
#mpd_password = ""
#mpd_connection_timeout = 5
#mpd_crossfade_time = 5

# http://www.boost.org/doc/libs/1_46_1/libs/regex/doc/html/boost_regex/syntax/perl_syntax.html
#random_exclude_pattern = "^(temp|midi_songs).*"
# }}}

# SYSTEM ENCODING {{{

system_encoding = "UTF-8"
# }}}

# INTERFACE {{{

# classic - alternative
user_interface = alternative 
progressbar_look = ⥦⥤━

header_visibility = yes
statusbar_visibility = yes

clock_display_seconds = no
display_volume_level = yes
display_bitrate = yes
display_remaining_time = no

song_list_format = {%a - }{%t}|{$8%f$9}$R{$3%l$9}
song_library_format = {%n - }{%t}|{%f}
song_columns_list_format = (40)[cyan]{t|f:Title} (20)[yellow]{a} (20)[red]{b} (7f)[magenta]{l}

## Alternative Interface
alternative_header_first_line_format = $b$1$aqqu$/a$9 {$4%a$9 - }{$7%t$9}|{$7%f$9} $1$atqq$/a$9$/b
alternative_header_second_line_format = {{$3%b$9}{ ($4%y$9)}}|{$3%D$9}

## Classic Interface
song_status_format = {{%a{ "%b"{ (%y)}} - }{%t}}|{%f}

## Display Modes
# Note: Possible display modes: classic, columns.
playlist_display_mode = columns
browser_display_mode = columns
search_engine_display_mode = columns
playlist_editor_display_mode = columns
# }}}

# COLORS {{{

colors_enabled = yes

main_window_color = blue

header_window_color = blue
alternative_ui_separator_color = black:b
volume_color = green
state_flags_color = cyan

progressbar_color = black:b
progressbar_elapsed_color = green:b
statusbar_color = white

#empty_tag_color = cyan
#state_line_color = default
#color1 = white
#color2 = green
#statusbar_time_color = default:b
#player_state_color = default:b
#window_border_color = green
#active_window_border = red
# }}}


#current_item_prefix = $(yellow)$r

#current_item_suffix = $/r$(end)

#current_item_inactive_column_prefix = $(white)$r

#current_item_inactive_column_suffix = $/r$(end)

#now_playing_prefix = $b

#now_playing_suffix = $/b

#browser_playlist_prefix = "$2playlist$9 "

#selected_item_prefix = $6

#selected_item_suffix = $9

#modified_item_prefix = $3> $9

##
## Note: attributes are not supported for the following variables.
##
#song_window_title_format = {%a - }{%t}|{%f}
##
## Note: Below variables are used for sorting songs in browser.  The sort mode
## determines how songs are sorted, and can be used in combination with a sort
## format to specify a custom sorting format.  Available values for
## browser_sort_mode are "type", "name", "mtime", "format" and "none".
##

#browser_sort_mode = type

#browser_sort_format = {%a - }{%t}|{%f} {%l}

##### various settings #####

##
## Note: Custom command that will be executed each time song changes. Useful for
## notifications etc.
##
#execute_on_song_change = ""

##
## Note: Custom command that will be executed each time player state
## changes. The environment variable MPD_PLAYER_STATE is set to the current
## state (either unknown, play, pause, or stop) for its duration.
##

#execute_on_player_state_change = ""

#playlist_show_mpd_host = no

#playlist_show_remaining_time = no

#playlist_shorten_total_times = no

#playlist_separate_albums = no


#discard_colors_if_item_is_selected = yes

#show_duplicate_tags = yes

#incremental_seeking = yes

#seek_time = 1

#volume_change_step = 2

#autocenter_mode = no

#centered_cursor = no

##
## Note: You can specify third character which will be used to build 'empty'
## part of progressbar.
##

## Available values: database, playlist.
default_place_to_search_in = database

#data_fetching_delay = yes

## Available values: artist, album_artist, date, genre, composer, performer.
##
media_library_primary_tag = artist

media_library_albums_split_by_date = yes

media_library_hide_album_dates = no

## Available values: wrapped, normal.
##
#default_find_mode = wrapped

#default_tag_editor_pattern = %n - %t


## Show the "Connected to ..." message on startup
#connected_message_on_startup = yes

#titles_visibility = yes

#header_text_scrolling = yes

#cyclic_scrolling = no


# LYRICS {{{

lyrics_fetchers = azlyrics, genius, musixmatch, sing365, metrolyrics, justsomelyrics, jahlyrics, plyrics, tekstowo, zeneszoveg, internet

follow_now_playing_lyrics = no

fetch_lyrics_for_current_song_in_background = no

store_lyrics_in_song_dir = no

generate_win32_compatible_filenames = yes
# }}}


#allow_for_physical_item_deletion = no

##
## Note: If you set this variable, ncmpcpp will try to get info from last.fm in
## language you set and if it fails, it will fall back to english. Otherwise it
## will use english the first time.
##
## Note: Language has to be expressed as an ISO 639 alpha-2 code.
##
#lastfm_preferred_language = en

#space_add_mode = add_remove

show_hidden_files_in_local_browser = yes

##
## How shall screen switcher work?
##
## - "previous" - switch between the current and previous screen.
## - "screen1,...,screenN" - switch between given sequence of screens.
##
## Screens available for use: help, playlist, browser, search_engine,
## media_library, playlist_editor, tag_editor, outputs, visualizer, clock,
## lyrics, last_fm.
##
#screen_switcher_mode = playlist, browser

##
## Note: You can define startup screen by choosing screen from the list above.
##
#startup_screen = playlist

##
## Note: You can define startup slave screen by choosing screen from the list
## above or an empty value for no slave screen.
##
#startup_slave_screen = ""

#startup_slave_screen_focus = no

##
## Default width of locked screen (in %).  Acceptable values are from 20 to 80.
##

#locked_screen_width_part = 50

#ask_for_locked_screen_width_part = yes

##
## Width of media_library screen columns
##

#media_library_column_width_ratio_two = 1:1

#media_library_column_width_ratio_three = 1:1:1

##
## Width of playlist_editor screen columns
##

#playlist_editor_column_width_ratio = 1:2

#jump_to_now_playing_song_at_start = yes

#ask_before_clearing_playlists = yes


## Available values: none, basic, extended, perl.
##
#regular_expressions = perl

##
## Note: if below is enabled, ncmpcpp will ignore leading "The" word while
## sorting items in browser, tags in media library, etc.
##
ignore_leading_the = yes

##
## Note: if below is enabled, ncmpcpp will ignore diacritics while searching and
## filtering lists. This takes an effect only if boost was compiled with ICU
## support.
##
#ignore_diacritics = no

#block_search_constraints_change_if_items_found = yes

#empty_tag_marker = <empty>

#tags_separator = " | "

#tag_editor_extended_numeration = no

#media_library_sort_by_mtime = no

enable_window_title = yes

## - 1 - use mpd built-in searching (no regexes, pattern matching)
## - 2 - use ncmpcpp searching (pattern matching with support for regexes, but
##       if your mpd is on a remote machine, downloading big database to process
##       it can take a while
## - 3 - match only exact values (this mode uses mpd function for searching in
##       database and local one for searching in current playlist)
search_engine_default_search_mode = 1

external_editor = nvim
use_console_editor = yes


# MOUSE {{{

mouse_support = yes
mouse_list_scroll_whole_page = no
lines_scrolled = 5
# }}}

# VISUALIZER {{{

visualizer_data_source = localhost:5555
visualizer_in_stereo = yes

visualizer_output_name = Visualizer feed

visualizer_sync_interval = 0

## Note: To enable spectrum frequency visualization you need to compile ncmpcpp
## with fftw3 support.
## Available values: spectrum, wave, wave_filled, ellipse.
visualizer_type = spectrum

visualizer_fps = 60
visualizer_autoscale = no
visualizer_look = ●▮
visualizer_color = blue, cyan, green, yellow, magenta, red


## Spectrum

visualizer_spectrum_smooth_look = yes
# 1-5
visualizer_spectrum_dft_size = 2
visualizer_spectrum_gain = 10

visualizer_spectrum_hz_min = 20
visualizer_spectrum_hz_max = 20000
# }}}

# DELAYS {{{

## Time of inactivity (in seconds) after playlist highlighting will be disabled
playlist_disable_highlight_delay = 5

## Defines how long messages are supposed to be visible.
message_delay_time = 5
# }}}

