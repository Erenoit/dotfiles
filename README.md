# Erenoit's Dotfiles
These are my dotfiles. All of them are work in progress and they always will be. I am open for contributions for improvements and bugfixes.

## TODO
- [ ] .config
    - [x] fcitx5
    - [ ] lazygit
    - [x] hypr(land)
    - [ ] Optimus Manager
    - [x] tmux
- [x] .icons
- [x] .themes
- [ ] .gitconfig


## Installation

1. You need to clone the repository:
	- For HTTPS:
    ```shell
    $ git clone https://gitlab.com/Erenoit/dotfiles.git
    ```

	- For ssh:
    ```shell
    $ git clone git@gitlab.com:Erenoit/dotfiles.git
    ```

2. To install:
    - If you want to install everything:
    ```shell
    $ rustow *
    ```

    - If you want to install one:
    ```shell
    $ rustow [name_of_selection]
    ```

3. Neovim plugin install:
    - Open Neovim
    - Wait for `Lazy.nvim` to install plugins
    - Restart Neovim
    - Wait for other plugins to install required things
    - Restart Neovim again and start using

## Dependencies
- The programs which use the config files
- Fonts:
    - [Iosevka](https://github.com/be5invis/iosevka) *Main Font*
    - [Joypixels](https://joypixels.com/) *Color Emoji*
    - [Mononoki Nerd Font](https://github.com/ryanoasis/nerd-fonts)
    - [Fira Code](https://github.com/tonsky/FiraCode)
    - [Sarasa Gothic](https://github.com/be5invis/Sarasa-Gothic) *Japanese Font*
- [Node.js](https://nodejs.org/en/#) and [yarn](https://yarnpkg.com/)
- [Python](https://www.python.org/)
- [Rustow](https://gitlab.com/Erenoit/rustow) or [GNU Stow](https://www.gnu.org/software/stow/)

## Deprecated
This `configs` are deprecated because I started to use another program:
- `alacritty`
- `dxhd`
- `kitty`
- `picom`
- `qtile`
- `rofi`
- `wezterm`
- `xmonad`

All of them should work normally (at least they were working when I was using), but they will not
get any updates in the future.
