vim.opt_local.list = false

vim.keymap.set("n", "K", [[<cmd>lua require("nabla").popup()<CR>]], { noremap = true, silent = true, buffer = true })
