vim.opt.shell = "$SHELL"
vim.opt.encoding = "UTF-8"
vim.opt.fileformat = "unix"
vim.opt.updatetime = 3000

-- Shell
vim.opt.shell = "/bin/zsh"

-- Clipboard Support
vim.opt.clipboard = "unnamedplus"

-- CMD Line Height
vim.opt.cmdheight = 0

-- Statusline
vim.opt.laststatus = 3

-- Syntax Highlighting
vim.opt.syntax = "on"
vim.opt.termguicolors = true
vim.opt.background = "dark"

-- Line Numbers
vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.cursorline = true
vim.cmd.highlight("CursorLineNR cterm=bold")
vim.opt.colorcolumn = "100"

-- listchars
vim.opt.list = true
vim.opt.listchars:append "space:⋅"
vim.opt.listchars:append "eol:↴"
vim.opt.listchars:append "trail:-"

-- fillchars
vim.opt.fillchars:append "eob:~"

-- Wrap
vim.opt.wrap = true

-- Folding
vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "nvim_treesitter#foldexpr()"
vim.opt.foldlevel = 20
vim.opt.foldlevelstart = 20

-- Mouse
vim.opt.mouse = "a"
vim.opt.mousescroll = "ver:3,hor:6"

-- Scroll Of
vim.opt.scrolloff = 7

-- Search
vim.opt.hlsearch = false
vim.opt.incsearch = true
vim.opt.ignorecase = true
vim.opt.smartcase = true

-- Backup
vim.opt.backup = false
vim.opt.writebackup = false
vim.opt.undofile = true

-- Backspace
vim.backspace = "indent,eol,start"

-- Whitespace
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true
vim.opt.smarttab = true
vim.opt.smartindent = true
vim.opt.autoindent = true
