-- Leader Key
vim.g.mapleader = " "

local keys = {
  ------------------------------------------------------------------------------------------------------------
  --|Mode|    Binding         |                  Command                   |             Options             |
  ------------------------------------------------------------------------------------------------------------

  -- Harpoon
  { "n", "<Leader>hh",       [[<cmd>lua require("harpoon.ui").toggle_quick_menu()<CR>]], { noremap = true, silent = true } },
  { "n", "<Leader>hm",       [[<cmd>lua require("harpoon.mark").add_file()<CR>]], { noremap = true, silent = true } },
  { "n", "<TAB>",            [[<cmd>lua require("harpoon.ui").nav_next()<CR>]], { noremap = true, silent = true } },
  { "n", "<S-TAB>",          [[<cmd>lua require("harpoon.ui").nav_prev()<CR>]], { noremap = true, silent = true } },
  -- Escape
  { "i", "jk",               "<ESC>",                                           { noremap = true, silent = true } },
  { "i", "kj",               "<ESC>",                                           { noremap = true, silent = true } },
  { "i", "jj",               "<ESC>",                                           { noremap = true, silent = true } },
  -- Paste Without Losing Paste Register
  { "x", "<Leader>p",        "\"_dP",                                           { noremap = true, silent = true } },
  -- Better Indenting
  { "v", "<",                "<gv",                                             { noremap = true, silent = true } },
  { "v", ">",                ">gv",                                             { noremap = true, silent = true } },
  { "v", "<A-h>",            "<gv",                                             { noremap = true, silent = true } },
  { "v", "<A-l>",            ">gv",                                             { noremap = true, silent = true } },
  { "v", "<A-Left>",         "<gv",                                             { noremap = true, silent = true } },
  { "v", "<A-Right>",        ">gv",                                             { noremap = true, silent = true } },
  { "n", "<",                "<<",                                              { noremap = true, silent = true } },
  { "n", ">",                ">>",                                              { noremap = true, silent = true } },
  { "n", "<A-h>",            "<<",                                              { noremap = true, silent = true } },
  { "n", "<A-l>",            ">>",                                              { noremap = true, silent = true } },
  { "n", "<A-Left>",         "<<",                                              { noremap = true, silent = true } },
  { "n", "<A-Right>",        ">>",                                              { noremap = true, silent = true } },
  { "i", "<A-h>",            "<ESC><<i",                                        { noremap = true, silent = true } },
  { "i", "<A-l>",            "<ESC>>>i",                                        { noremap = true, silent = true } },
  { "i", "<A-Left>",         "<ESC><<i",                                        { noremap = true, silent = true } },
  { "i", "<A-Right>",        "<ESC>>>i",                                        { noremap = true, silent = true } },
  -- Move selected line/block
  { "n", "<A-k>",            "<cmd>move -2<CR>=<CR>",                           { noremap = true, silent = true } },
  { "n", "<A-j>",            "<cmd>move +1<CR>=<CR>",                           { noremap = true, silent = true } },
  { "i", "<A-k>",            "<ESC><cmd>move -2<CR>=<CR>",                      { noremap = true, silent = true } },
  { "i", "<A-j>",            "<ESC><cmd>move +1<CR>=<CR>",                      { noremap = true, silent = true } },
  { "x", "<A-k>",            ":move '<-2<CR>gv=gv",                             { noremap = true, silent = true } },
  { "x", "<A-j>",            ":move '>+1<CR>gv=gv",                             { noremap = true, silent = true } },
  -- Lazygit
  { "n", "<Leader>lg",       "<cmd>LazyGit<CR>",                                { noremap = true, silent = true } },
  { "n", "<Leader>lc",       "<cmd>LazyGitConfig<CR>",                          { noremap = true, silent = true } },
  -- LSP
  { "n", "gD",               "<cmd>lua vim.lsp.buf.declaration()<CR>",          { noremap = true, silent = true } },
  { "n", "gi",               "<cmd>lua vim.lsp.buf.implemantation()<CR>",       { noremap = true, silent = true } },
  { "n", "<Leader>D",        "<cmd>lua vim.lsp.buf.type_definition()<CR>",      { noremap = true, silent = true } },
  { "n", "gr",               "<cmd>lua vim.lsp.buf.references()<CR>",           { noremap = true, silent = true } },
  { "n", "<Leader>f",        "<cmd>lua vim.lsp.buf.formatting()<CR>",           { noremap = true, silent = true } },
  -- Lspsaga
  { "n", "<Leader>tt",       "<cmd>Lspsaga term_toggle<CR>",                    { noremap = true, silent = true } },
  { "t", "<Leader>tt",       "<cmd>Lspsaga term_toggle<CR>",                    { noremap = true, silent = true } },
  { "n", "<Leader>rn",       "<cmd>Lspsaga rename<CR>",                         { noremap = true, silent = true } },
  { "n", "<Leader>ca",       "<cmd>Lspsaga code_action<CR>",                    { noremap = true, silent = true } },
  { "v", "<Leader>ca",       "<cmd>Lspsaga code_action<CR>",                    { noremap = true, silent = true } },
  { "n", "<Leader>ci",       "<cmd>Lspsaga incoming_calls<CR>",                 { noremap = true, silent = true } },
  { "n", "<Leader>co",       "<cmd>Lspsaga outgoing_calls<CR>",                 { noremap = true, silent = true } },
  { "n", "<Leader>o",        "<cmd>Lspsaga outline<CR>",                        { noremap = true, silent = true } },
  { "n", "gd",               "<cmd>Lspsaga peek_definition<CR>",                { noremap = true, silent = true } },
  { "n", "K",                "<cmd>Lspsaga hover_doc<CR>",                      { noremap = true, silent = true } },
  { "n", "[d",               "<cmd>Lspsaga diagnostic_jump_prev<CR>",           { noremap = true, silent = true } },
  { "n", "]d",               "<cmd>Lspsaga diagnostic_jump_next<CR>",           { noremap = true, silent = true } },
  -- COQ
  { "i", "<esc>",            [[pumvisible() ? "<c-e><esc>" : "<esc>"]],         { noremap = true, expr = true } },
  { "i", "<c-c>",            [[pumvisible() ? "<c-e><c-c>" : "<c-c>"]],         { noremap = true, expr = true } },
  { "i", "<tab>",            [[pumvisible() ? "<c-n>" : "<tab>"]],              { noremap = true, expr = true } },
  { "i", "<s-tab>",          [[pumvisible() ? "<c-p>" : "<bs>"]],               { noremap = true, expr = true } },
  -- DAP
  { "n", "<leader>db",       "<cmd>lua require('dap').toggle_breakpoint()<CR>", { noremap = true, silent = true } },
  { "n", "<leader>dB",       "<cmd>lua require('dap').set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>", { noremap = true, silent = true } },
  { "n", "<leader>dr",       "<cmd>lua require('dap').repl.open()<CR>",         { noremap = true, silent = true } },
  { "n", "<leader>dc",       "<cmd>lua require('dap').continue()<CR>",          { noremap = true, silent = true } },
  { "n", "<leader>do",       "<cmd>lua require('dap').step_over()<CR>",         { noremap = true, silent = true } },
  { "n", "<leader>di",       "<cmd>lua require('dap').step_into()<CR>",         { noremap = true, silent = true } },
  { "n", "<leader>du",       "<cmd>lua require('dap').step_out()<CR>",          { noremap = true, silent = true } },
  { "n", "<leader>dt",       "<cmd>lua require('dap').run_to_cursor()<CR>",     { noremap = true, silent = true } },
  { "n", "<leader>dv",       "<cmd>lua require('dap').ui.variables()<CR>",      { noremap = true, silent = true } },
  { "n", "<leader>dk",       "<cmd>lua require('dap').ui.locals()<CR>",         { noremap = true, silent = true } },
  { "n", "<leader>dd",       "<cmd>lua require('dapui').toggle()<CR>",          { noremap = true, silent = true } },
  -- Oil
  { "n", "<Leader>ee",       [[<cmd>lua require("oil").toggle_float()<CR>]],    { noremap = true, silent = true } },
  { "n", "<Leader>ex",       [[<cmd>lua require("oil").discard_all_changes()<CR>]], { noremap = true, silent = true } },
  { "n", "<Leader>eh",       [[<cmd>lua require("oil").toggle_hidden()<CR>]],   { noremap = true, silent = true } },
  -- Flote
  { "n", "<Leader>n",        "<cmd>Flote<CR>",                                  { noremap = true, silent = true } },
  -- Markdown
  { "n", "<Leader>m",        "<cmd>MarkdownPreviewToggle<CR>",                  { noremap = true, silent = true } },
  -- Telescope
  { "n", "<Leader>tff",      "<cmd>Telescope find_files<CR>",                   { noremap = true, silent = true } },
  { "n", "<Leader>tfh",      "<cmd>Telescope oldfiles<CR>",                     { noremap = true, silent = true } },
  { "n", "<Leader>tg",       "<cmd>Telescope live_grep<CR>",                    { noremap = true, silent = true } },
  { "n", "<Leader>tb",       "<cmd>Telescope buffers<CR>",                      { noremap = true, silent = true } },
  { "n", "<Leader>th",       "<cmd>Telescope halp_tags<CR>",                    { noremap = true, silent = true } },
  { "n", "<Leader>tc",       "<cmd>Telescope colorscheme<CR>",                  { noremap = true, silent = true } },
  { "n", "<Leader>tm",       "<cmd>Telescope man_pages<CR>",                    { noremap = true, silent = true } },
  { "n", "<Leader>tw",       [[<cmd>lua require("telescope.builtin").grep_string({ search = vim.fn.expand("<cword>")})]], { noremap = true, silent = true } },
  { "n", "<Leader>tW",       [[<cmd>lua require("telescope.builtin").grep_string({ search = vim.fn.expand("<cword>")})]], { noremap = true, silent = true } },
  -- Telescope Plugins
  { "n", "<Leader>tfr",      "<cmd>Telescope frecency<CR>",                     { noremap = true, silent = true } },
  { "n", "<Leader>tn",       "<cmd>Telescope node_modules list<CR>",            { noremap = true, silent = true } },
  { "n", "<Leader>tm",       "<cmd>Telescope media_files<CR>",                  { noremap = true, silent = true } },
  { "n", "<Leader>te",       "<cmd>Telescope emoji<CR>",                        { noremap = true, silent = true } },
  { "n", "<Leader>tdc",      "<cmd>Telescope dap commands<CR>",                 { noremap = true, silent = true } },
  { "n", "<Leader>tdC",      "<cmd>Telescope dap configurations<CR>",           { noremap = true, silent = true } },
  { "n", "<Leader>tdl",      "<cmd>Telescope dap list_breakpoints<CR>",         { noremap = true, silent = true } },
  { "n", "<Leader>tdv",      "<cmd>Telescope dap variables<CR>",                { noremap = true, silent = true } },
  { "n", "<Leader>tdf",      "<cmd>Telescope dap frames<CR>",                   { noremap = true, silent = true } },
  -- Toruble
  { "n", "<Leader>tt",       "<cmd>TroubleToggle<CR>",                          { noremap = true, silent = true } },
  -- TODO: tn -> next with skip_group and jump
  -- whichkey
  { "n", "<Leader><Leader>", "<cmd>WhichKey<CR>",                               { noremap = true, silent = true } },

}

-- Init Kepmaps
for i = 1, #keys do
  vim.api.nvim_set_keymap(keys[i][1], keys[i][2], keys[i][3], keys[i][4])
end




---------------------------------------------------------------------------------------------------
---- Gitsigns
--  {"n", "]c", "&diff ? ']c' : '<cmd>lua require\"gitsigns.actions\".next_hunk()<CR>'", { noremap = true, expr = true }},
--  {"n", "[c", "&diff ? '[c' : '<cmd>lua require\"gitsigns.actions\".prev_hunk()<CR>'", { noremap = true, expr = true }},
--
--  {"n", "<leader>hs", '<cmd>lua require"gitsigns".stage_hunk()<CR>', { noremap = true, expr = true }},
--  {"v", "<leader>hs", '<cmd>lua require"gitsigns".stage_hunk({vim.fn.line("."), vim.fn.line("v")})<CR>', { noremap = true, expr = true }},
--  {"n", "<leader>hu", '<cmd>lua require"gitsigns".undo_stage_hunk()<CR>', { noremap = true, expr = true }},
--  {"n", "<leader>hr", '<cmd>lua require"gitsigns".reset_hunk()<CR>', { noremap = true, expr = true }},
--  {"v", "<leader>hr", '<cmd>lua require"gitsigns".reset_hunk({vim.fn.line("."), vim.fn.line("v")})<CR>', { noremap = true, expr = true }},
--  {"n", "<leader>hR", '<cmd>lua require"gitsigns".reset_buffer()<CR>', { noremap = true, expr = true }},
--  {"n", "<leader>hp", '<cmd>lua require"gitsigns".preview_hunk()<CR>', { noremap = true, expr = true }},
--  {"n", "<leader>hb", '<cmd>lua require"gitsigns".blame_line(true)<CR>', { noremap = true, expr = true }},
--  {"n", "<leader>hS", '<cmd>lua require"gitsigns".stage_buffer()<CR>', { noremap = true, expr = true }},
--  {"n", "<leader>hU", '<cmd>lua require"gitsigns".reset_buffer_index()<CR>', { noremap = true, expr = true }},
--
--  -- Text objects
--  {"o", "ih", ':<C-U>lua require"gitsigns.actions".select_hunk()<CR>', { noremap = true, expr = true }},
--  {"x", "ih",  ':<C-U>lua require"gitsigns.actions".select_hunk()<CR>', { noremap = true, expr = true }},
