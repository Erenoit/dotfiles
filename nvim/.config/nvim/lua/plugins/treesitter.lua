return {
  { "nvim-treesitter/nvim-treesitter",
    build = ":TSUpdate",
    config = function()
      require("nvim-treesitter.configs").setup {
        -- One of "all", "maintained" (parsers with maintainers), or a list of languages
        ensure_installed = "all",
        auto_install = true,

        highlight = {
          enable = true,
          custom_captures = {
            -- Highlight the @foo.bar capture group with the "Identifier" highlight group.
            ["foo.bar"] = "Identifier",
          },
          -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
          -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
          -- Using this option may slow down your editor, and you may see some duplicate highlights.
          -- Instead of true it can also be a list of languages
          additional_vim_regex_highlighting = false,
        },

        -- nvim-ts-autotag plugin
        autotag = {
          enable = true,
        },

        -- nvim-ts-refactor plugin
        refactor = {
          highlight_definitions = { enable = true },
          highlight_current_scope = { enable = false },
          smart_rename = {
            enable = true,
            keymaps = {
              smart_rename = "grr",
            },
          },
          navigation = {
            enable = true,
            keymaps = {
              goto_definition = "gnd",
              list_definitions = "gnD",
              list_definitions_toc = "gO",
              goto_next_usage = "<a-*>",
              goto_previous_usage = "<a-#>",
            },
          },
        },

        -- nvim-autopairs plugin
        autopairs = {
          enable = true,
        },
      }
    end,
  },
  {
    url = "https://gitlab.com/HiPhish/rainbow-delimiters.nvim.git",
    dependencies = "nvim-treesitter/nvim-treesitter",
    config = function()
      vim.api.nvim_set_hl(0, "RainbowDelimiterRed",    {fg="#E06C75"})
      vim.api.nvim_set_hl(0, "RainbowDelimiterYellow", {fg="#E5C07B"})
      vim.api.nvim_set_hl(0, "RainbowDelimiterGreen",  {fg="#98C379"})
      vim.api.nvim_set_hl(0, "RainbowDelimiterCyan",   {fg="#56B6C2"})
      vim.api.nvim_set_hl(0, "RainbowDelimiterBlue",   {fg="#61AFEF"})
      vim.api.nvim_set_hl(0, "RainbowDelimiterViolet", {fg="#C678DD"})

      require("rainbow-delimiters.setup")({
        strategy = {
          [''] = require("rainbow-delimiters").strategy["global"],
        },
        query = {
          [''] = "rainbow-delimiters",
          lua = "rainbow-blocks",
        },
        highlight = {
          "RainbowDelimiterRed",
          "RainbowDelimiterYellow",
          "RainbowDelimiterGreen",
          "RainbowDelimiterCyan",
          "RainbowDelimiterBlue",
          "RainbowDelimiterViolet",
          --"RainbowDelimiterOrange",
        },
      })
    end,
  },
  { "windwp/nvim-ts-autotag",
    dependencies = "nvim-treesitter/nvim-treesitter",
  },
  { "nvim-treesitter/nvim-treesitter-refactor",
    dependencies = "nvim-treesitter/nvim-treesitter",
  },
}
