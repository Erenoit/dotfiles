return {
  "hoob3rt/lualine.nvim",
  dependencies = "kyazdani42/nvim-web-devicons",
  priority = 999,
  config = function()
    local colors = {
      bg       = "#202328",
      fg       = "#bbc2cf",
      yellow   = "#ECBE7B",
      cyan     = "#008080",
      darkblue = "#081633",
      green    = "#98be65",
      orange   = "#FF8800",
      violet   = "#a9a1e1",
      magenta  = "#c678dd",
      blue     = "#51afef",
      red      = "#ec5f67",
      white    = "#ffffff"
    }

    --local colors = require("onedarkpro").get_colors("onedark")

    local conditions = {
      buffer_not_empty = function() return vim.fn.empty(vim.fn.expand("%:t")) ~= 1 end,
      hide_in_width = function() return vim.fn.winwidth(0) > 80 end,
      check_git_workspace = function()
        local filepath = vim.fn.expand("%:p:h")
        local gitdir = vim.fn.finddir(".git", filepath .. ";")
        return gitdir and #gitdir > 0 and #gitdir < #filepath
      end
    }

    require("lualine").setup({
      options = {
        theme = "tokyonight",
        section_separators = {
          --left  = "",
          --right = ""
          left  = "",
          right = ""
        },
        --component_separators = {"", ""}
        component_separators = {
          --left  = "|",
          --right = "|"
          left  = "",
          right = ""
        }
      },
      sections = {
        lualine_a = {
          {
            "filename",
            icon = require("nvim-web-devicons").get_icon(vim.bo.filetype)
          },
          {
            -- filesize component
            function()
              local function format_file_size(file)
                local size = vim.fn.getfsize(file)
                if size <= 0 then return "" end
                local sufixes = { "b", "k", "m", "g" }
                local i = 1
                while size > 1024 do
                  size = size / 1024
                  i = i + 1
                end
                return string.format("%.1f%s", size, sufixes[i])
              end
              local file = vim.fn.expand("%:p")
              if string.len(file) == 0 then return "" end
              return format_file_size(file)
            end,
            condition = conditions.buffer_not_empty
          } },
        lualine_b = {
          "branch",
          {
            "diff",
            symbols = { added = " ", modified = "󱗜 ", removed = " " },
            color_added = colors.green,
            color_modified = colors.orange,
            color_removed = colors.red,
            condition = conditions.hide_in_width
          } },
        lualine_c = {},
        lualine_x = {
          --        {
          --          function() return " " end
          --        },
          -- Lsp server name .
          { function()
            local msg = "No Active Lsp"
            local buf_ft = vim.api.nvim_buf_get_option(0, "filetype")
            local clients = vim.lsp.get_active_clients()
            if next(clients) == nil then return msg end
            for _, client in ipairs(clients) do
              local filetypes = client.config.filetypes
              if filetypes and vim.fn.index(filetypes, buf_ft) ~= -1 then
                return client.name
              end
            end
            return msg
          end,
            --        icon = "  ",
            color = { fg = colors.white, gui = "bold" }
          }
        },
        lualine_y = { {
          "diagnostics",
          sources = { "nvim_lsp" },
          symbols = { error = " ", warn = " ", info = " " },
          color_error = colors.red,
          color_warn = colors.yellow,
          color_info = colors.cyan
        } },
        lualine_z = {
          "location",
          "progress"
        }
      },
    })
  end,
}
