return {
  "norcalli/nvim-colorizer.lua",
  config = function()
    require("colorizer").setup(
      { "*", },
      {
        RGB      = true,
        RRGGBB   = true,
        RRGGBBAA = true,
        names    = false,
        rgb_fn   = true,
        hsl_fn   = true,
        css      = true,
        css_fn   = true,
      }
    )
  end,
}
