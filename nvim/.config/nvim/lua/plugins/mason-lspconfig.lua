return {
  "williamboman/mason-lspconfig.nvim",
  dependencies = { "williamboman/mason.nvim", "neovim/nvim-lspconfig" },
  config = function()
    require("mason-lspconfig").setup({
      ensure_installed = {
        "awk_ls",
        "asm_lsp",
        "bashls",
        "clangd",
        "clojure_lsp",
        "cmake",
        "cssls",
        "fortls",
        "gopls",
        "graphql",
        "hls",
        "html",
        "jdtls",
        "jsonls",
        "kotlin_language_server",
        "lemminx",
        "ltex",
        "omnisharp",
        "pyright",
        "rust_analyzer",
        "slint_lsp",
        "solargraph",
        "sqlls",
        "lua_ls",
        "svelte",
        "taplo",
        "texlab",
        "tsserver",
        "vimls",
        "volar",
        "yamlls",
        "zls",
      },
      automatic_installation = false,
    })

    require("mason-lspconfig").setup_handlers({
      function(server_name)
        require("lspconfig")[server_name].setup(require("coq").lsp_ensure_capabilities())
      end,
      ["rust_analyzer"] = function()
        require("rust-tools").setup({
          tools = {
            -- options right now: termopen / quickfix
            executor = require("rust-tools/executors").termopen,

            -- callback to execute once rust-analyzer is done initializing the workspace
            -- The callback receives one parameter indicating the `health` of the server: "ok" | "warning" | "error"
            on_initialized = nil,

            reload_workspace_from_cargo_toml = true,

            inlay_hints = {
              auto = true,
              only_current_line = false,
              show_parameter_hints = true,
              parameter_hints_prefix = "<- ",
              other_hints_prefix = "=> ",
              max_len_align = false,
              max_len_align_padding = 1,
              right_align = false,
              right_align_padding = 7,
              highlight = "Comment",
            },

            hover_actions = {
              border = {
                { "╭", "FloatBorder" },
                { "─", "FloatBorder" },
                { "╮", "FloatBorder" },
                { "│", "FloatBorder" },
                { "╯", "FloatBorder" },
                { "─", "FloatBorder" },
                { "╰", "FloatBorder" },
                { "│", "FloatBorder" },
              },
              auto_focus = false,
            },

            -- settings for showing the crate graph based on graphviz and the dot
            -- command
            crate_graph = {
              -- Backend used for displaying the graph
              -- see: https://graphviz.org/docs/outputs/
              -- default: x11
              backend = "x11",
              -- where to store the output, nil for no output stored (relative
              -- path from pwd)
              -- default: nil
              output = nil,
              -- true for all crates.io and external crates, false only the local
              -- crates
              -- default: true
              full = true,

              -- List of backends found on: https://graphviz.org/docs/outputs/
              -- Is used for input validation and autocompletion
              -- Last updated: 2021-08-26
              enabled_graphviz_backends = {
                "bmp",
                "cgimage",
                "canon",
                "dot",
                "gv",
                "xdot",
                "xdot1.2",
                "xdot1.4",
                "eps",
                "exr",
                "fig",
                "gd",
                "gd2",
                "gif",
                "gtk",
                "ico",
                "cmap",
                "ismap",
                "imap",
                "cmapx",
                "imap_np",
                "cmapx_np",
                "jpg",
                "jpeg",
                "jpe",
                "jp2",
                "json",
                "json0",
                "dot_json",
                "xdot_json",
                "pdf",
                "pic",
                "pct",
                "pict",
                "plain",
                "plain-ext",
                "png",
                "pov",
                "ps",
                "ps2",
                "psd",
                "sgi",
                "svg",
                "svgz",
                "tga",
                "tiff",
                "tif",
                "tk",
                "vml",
                "vmlz",
                "wbmp",
                "webp",
                "xlib",
                "x11",
              },
            },
          },
          -- all the opts to send to nvim-lspconfig
          -- these override the defaults set by rust-tools.nvim
          -- see https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#rust_analyzer
          server = {
            -- standalone file support
            -- setting it to false may improve startup time
            standalone = true,
            settings = {
              ["rust-analyzer"] = {
                rustfmt = {
                  extraArgs = { "+nightly" },
                },
                check = {
                  command = "clippy",
                },
                diagnostics = {
                  disabled = { "inactive-code" },
                },
              }
            },
          }, -- rust-analyer options
          --dap = {},
        })
      end,
      ["lua_ls"] = function()
        require("lspconfig").lua_ls.setup({
          settings = {
            Lua = {
              diagnostics = {
                globals = { "vim" },
              },
            }
          }
        })
      end,
    })
  end,
}
