return {
  "JellyApple102/flote.nvim",
  lazy = true,
  cmd = "Flote",
  config = function()
    require("flote").setup({
      q_to_quit = true,
      window_style = "minimal",
      window_border = "solid",
    })
  end,
}
