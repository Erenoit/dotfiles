return {
  "ThePrimeagen/harpoon",
  dependencies = "nvim-lua/plenary.nvim",
  config = function()
    require("harpoon").setup({
      excluded_filetypes = { "harpoon", "oil" },

    })
  end,
}
