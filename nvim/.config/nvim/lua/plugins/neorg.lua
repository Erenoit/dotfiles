return {
  "nvim-neorg/neorg",
  build = ":Neorg sync-parsers",
  dependencies = { "nvim-lua/plenary.nvim", "nvim-treesitter/nvim-treesitter" },
  lazy = true,
  ft = "norg",
  cmd = "Neorg",
  config = function()
    require("neorg").setup({
      load = {
        ["core.concealer"] = {
          config = {
            icon_preset = "diamond",
            init_open_folds = "never",
          },
        },
        ["core.qol.todo_items"] = {
          config = {
            create_todo_parents = true,
            order = { "undone", "pending", "done" },
          },
        },
        ["core.dirman"] = {
          config = {
            workspaces = {
              school = "~/Notes/school",
              linux = "~/Notes/linux",
              personal = "~/Notes/personal",
            },
          },
        },
        -- Only Enablers
        ["core.clipboard.code-blocks"] = {},
        ["core.esupports.hop"] = {},
        ["core.esupports.indent"] = {},
        ["core.esupports.metagen"] = {},
        ["core.itero"] = {},
        ["core.journal"] = {},
        ["core.looking-glass"] = {},
        ["core.pivot"] = {},
        ["core.promo"] = {},
        ["core.qol.toc"] = {},
        ["core.tangle"] = {},
        --["core.ui.calendar"] = {},
        -- Dev deps
        ["core.autocommands"] = {},
        ["core.clipboard"] = {},
        ["core.dirman.utils"] = {},
        ["core.fs"] = {},
        ["core.highlights"] = {},
        ["core.keybinds"] = {},
        ["core.integrations.treesitter"] = {},
        ["core.mode"] = {},
        ["core.neorgcmd"] = {},
        ["core.neorgcmd.commands.return"] = {},
        ["core.ui"] = {},
      },
    })
  end,
}
