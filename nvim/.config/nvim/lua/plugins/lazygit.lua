return {
  "kdheepak/lazygit.nvim",
  dependencies = "nvim-telescope/telescope.nvim",
  lazy = true,
  cmd = "LazyGit",
  config = function()
    require("telescope").load_extension("lazygit")
  end,
}
