return {
  { "nvim-telescope/telescope.nvim",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-telescope/telescope-media-files.nvim",
      "nvim-telescope/telescope-fzf-native.nvim",
    },
    lazy = true,
    cmd = "Telescope",
    config = function()
      require("telescope").setup({
        defaults = {
          -- Default configuration for telescope goes here:
          -- config_key = value,
          -- ..
        },
        pickers = {
          -- Default configuration for builtin pickers goes here:
          -- picker_name = {
          --   picker_config_key = value,
          --   ...
          -- }
          -- Now the picker_config_key will be applied every time you call this
          -- builtin picker
        },
        extensions = {
          fzf = {
            fuzzy = true, -- false will only do exact matching
            override_generic_sorter = true, -- override the generic sorter
            override_file_sorter = true, -- override the file sorter
            case_mode = "smart_case", -- "smart_case" or "ignore_case" or "respect_case"
          },
          frecency = {
            --db_root = "home/erenoit/.config/nvim/lua/nv-telescope/frecency-db",
            show_scores = false,
            show_unindexed = true,
            ignore_patterns = { "*.git/*", "*/tmp/*" },
            disable_devicons = false,
            workspaces = {
              ["conf"]    = vim.env.XDG_CONFIG_HOME,
              ["data"]    = vim.env.XDG_DATA_HOME,
              ["project"] = vim.env.HOME .. "Projects",
              ["notes"]   = vim.env.HOME .. "Notes"
            }
          },
          media_files = {
            filetypes = { "png", "webp", "webm", "jpg", "jpeg", "pdf", "mp4" },
            find_cmd = "rg"
          },
        }
      })
    end,
  },
  { "nvim-telescope/telescope-fzf-native.nvim",
    build = "make",
    lazy = true,
    config = function()
      require("telescope").load_extension("fzf")
    end
  },
  { "nvim-telescope/telescope-media-files.nvim",
    lazy = true,
    config = function()
      require("telescope").load_extension("media_files")
    end
  },
}
