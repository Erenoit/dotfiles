return {
  "rcarriga/nvim-notify",
  priority = 10000,
  config = function()
    require("notify").setup({
      max_height = math.max(vim.api.nvim_win_get_height(0) / 2, 10),
      max_width = math.max(vim.api.nvim_win_get_width(0) / 2, 30),
      stages = "slide",
      top_down = true,
    })

    vim.notify = require("notify")
  end,
}
