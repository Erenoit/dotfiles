return {
  "iamcco/markdown-preview.nvim",
  lazy = true,
  event = "BufRead *.md",
  build = "cd app && yarn install",
  config = function()
    vim.g.mkdp_auto_start = 0
    vim.g.mkdp_auto_close = 1
    vim.g.mkdp_refresh_slow = 0
    vim.g.mkdp_command_for_global = 0
    vim.g.mkdp_open_to_the_world = 0
    --vim.g.mkdp_open_ip = ""
    vim.g.mkdp_browser = "qutebrowser"
    -- preview page title
    -- ${name} will be replace with the file name
    vim.g.mkdp_page_title = "「${name}」"
    -- recognized filetypes
    -- these filetypes will have MarkdownPreview... commands
    vim.g.mkdp_filetypes = { "markdown" }
    vim.g.mkdp_highlight_css = vim.env.XDG_CONFIG_HOME .. "/nvim/other/highlight.js/src/styles/tokyo-night-dark.css"
    -- set default theme (dark or light)
    -- By default the theme is define according to the preferences of the system
    vim.g.mkdp_theme = "dark"
  end,
}
