return {
  "saecki/crates.nvim",
  dependencies = "nvim-lua/plenary.nvim",
  lazy = true,
  event = "BufRead Cargo.toml",
  config = function()
    require("crates").setup({
      src = {
        coq = {
          enabled = true,
          name = "crates.nvim",
        },
      },
    })

    require('crates').show()
  end,
}
