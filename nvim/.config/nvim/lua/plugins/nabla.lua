return {
  "jbyuki/nabla.nvim",
  lazy = true,
  ft = { "norg", "markdown" },
}
