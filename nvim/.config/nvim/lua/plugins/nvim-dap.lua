return {
  { "mfussenegger/nvim-dap",
    -- TODO: does only `lazy = true` is enough? Does it need condition?
    lazy = true,
    config = function()
      require("dap").adapters.codelldb = {
        type = "server",
        --host = '127.0.0.1',
        port = 13000,
        executable = {
          command = vim.env.XDG_CONFIG_HOME .. "/nvim/other/codelldb-x86_64-linux-1.9.0/extension/adapter/codelldb",
          args = { "--port", "13000" },
        }
      }

      require("dap").configurations.rust = {
        {
          name = "Debug executable 'main'",
          type = "codelldb",
          request = "launch",
          program = function()
            return vim.fn.input("Path to executable: ", vim.fn.getcwd() .. "/target/debug/", "file")
          end,
          cwd = "${workspaceFolder}",
          stopOnEntry = false,
        },
      }

      require("dap").listeners.after.event_initialized["dapui_config"] = function()
        vim.api.nvim_command("NvimTreeClose")
        require("dapui").open()
      end
      require("dap").listeners.before.event_terminated["dapui_config"] = function()
        require("dapui").close()
        -- find a way to not focus on the tree
        vim.api.nvim_command("NvimTreeOpen")
      end
      require("dap").listeners.before.event_exited["dapui_config"] = function()
        require("dapui").close()
        -- find a way to not focus on the tree
        vim.api.nvim_command("NvimTreeOpen")
      end
    end,
  },
  { "rcarriga/nvim-dap-ui",
    dependencies = "mfussenegger/nvim-dap",
    -- TODO: does only `lazy = true` is enough? Does it need condition?
    lazy = true,
    config = function()
      require("dapui").setup({
        icons = {
          expanded = "▾",
          collapsed = "▸",
        },
        mappings = {
          -- Use a table to apply multiple mappings
          expand = { "<CR>", "<2-LeftMouse>" },
          open = "o",
          remove = "d",
          edit = "e",
          repl = "r",
        },
        sidebar = {
          elements = {
            -- You can change the order of elements in the sidebar
            "scopes",
            "breakpoints",
            "stacks",
            "watches",
          },
          width = 40,
          position = "left", -- Can be "left" or "right"
        },
        tray = {
          elements = {
            "repl",
          },
          height = 10,
          position = "bottom", -- Can be "bottom" or "top"
        },
        floating = {
          max_height = nil, -- These can be integers or a float between 0 and 1.
          max_width = nil,  -- Floats will be treated as percentage of your screen.
          mappings = {
            close = { "q", "<Esc>" },
          },
        },
        windows = { indent = 1 },
      })
    end,
  },
  { "theHamsta/nvim-dap-virtual-text",
    dependencies = "mfussenegger/nvim-dap",
    -- TODO: does only `lazy = true` is enough? Does it need condition?
    lazy = true,
    config = function()
      require("nvim-dap-virtual-text").setup({
        enabled = true,
        enabled_commands = true,
        highlight_changed_variables = true,
        highlight_new_as_changed = false,
        show_stop_reason = true,
        commented = false,
        only_first_definition = true,
        all_references = false,
        --- A callback that determines how a variable is displayed or whether it should be omitted
        --- @param variable Variable https://microsoft.github.io/debug-adapter-protocol/specification#Types_Variable
        --- @param buf number
        --- @param stackframe dap.StackFrame https://microsoft.github.io/debug-adapter-protocol/specification#Types_StackFrame
        --- @param node userdata tree-sitter node identified as variable definition of reference (see `:h tsnode`)
        --- @return string|nil A text how the virtual text should be displayed or nil, if this variable shouldn't be displayed
        display_callback = function(variable, _buf, _stackframe, _node)
          return variable.name .. ' = ' .. variable.value
        end,
        -- experimental features:
        virt_text_pos = "eol", -- position of virtual text, see `:h nvim_buf_set_extmark()`
        all_frames = false,    -- show virtual text for all stack frames not only current. Only works for debugpy on my machine.
        virt_lines = false,    -- show virtual lines instead of virtual text (will flicker!)
        virt_text_win_col = nil
      })
    end,
  },
}
