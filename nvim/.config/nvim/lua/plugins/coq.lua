return {
  { "ms-jpq/coq_nvim",
    dependencies = { "ms-jpq/coq.thirdparty", "ms-jpq/coq.artifacts" },
    branch = "coq",
    build = ":COQdeps",
    lazy = true,
    event = "InsertEnter",
    config = function()
      vim.g.coq_settings = {
        auto_start = "shut-up",
        display = {
          pum = {
            fast_close = false,
          },
        },
        keymap = { recommended = false },
      }
    end,
  },
  { "ms-jpq/coq.thirdparty",
    branch = "3p",
    lazy = true,
    config = function()
      require("coq_3p")({
        {
          src = "repl",
          sh = "zsh",
          shell = { p = "perl", n = "node", },
          max_lines = 99,
          deadline = 500,
          unsafe = { "rm", "poweroff", "mv", }
        },
        {
          src = "nvimlua",
          short_name = "nLUA",
          conf_only = true
        },
        {
          src = "figlet",
          short_name = "BIG"
        },
        {
          src = "dap"
        },
      })
    end,
  },
  { "ms-jpq/coq.artifacts",
    lazy = true,
    branch = "artifacts",
  },
}
