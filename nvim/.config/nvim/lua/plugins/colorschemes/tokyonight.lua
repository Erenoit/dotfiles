return {
  "folke/tokyonight.nvim",
  priority = 1000,
  config = function()
    require("tokyonight").setup({
      style = "night",
      transparent = true,
      terminal_colors = true,
      styles = {
        comments = "italic",
        keywords = "italic,bold",
        functions = "bold",
        variables = "NONE",

        sidebars = "dark",
        floats = "dark",
      },
      sidebars = { "qf", "help", "packer", "nvim-tree" },
      day_brightness = 0.3,
      hide_inactive_statusline = true,
      dim_inactive = true,
      lualine_bold = true,
      ---@diagnostic disable-next-line: unused-local
      on_colors = function(colors)
      end,
      ---@diagnostic disable-next-line: unused-local
      on_highlights = function(highlights, colors)
      end,
    })
  end,
}
