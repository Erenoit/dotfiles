return {
  require("plugins.colorschemes.catppuccin"),
  require("plugins.colorschemes.kanagawa"),
  require("plugins.colorschemes.material"),
  require("plugins.colorschemes.onedarkpro"),
  require("plugins.colorschemes.tokyonight"),
}
