return {
  "olimorris/onedarkpro.nvim",
  priority = 1000,
  config = function()
    require('onedarkpro').setup({
      theme = "onedark", -- onedark, onelight
      colors = { -- Override default colors

      },
      hlgroups = { -- Override default highlight groups

      },
      styles = { -- NONE, italic, bold, underline
        comments  = "italic", -- Style that is applied to comments
        functions = "bold", -- Style that is applied to functions
        variables = "NONE", -- Style that is applied to variables
        strings   = "NONE", -- Style that is applied to strings
        keywords  = "italic", -- Style that is applied to keywords
      },
      options = {
        bold                 = true, -- Use the themes opinionated bold styles?
        italic               = true, -- Use the themes opinionated italic styles?
        underline            = true, -- Use the themes opinionated underline styles?
        undercurl            = true, -- Use the themes opinionated undercurl styles?
        highlight_cursorline = true, -- Use cursorline highlighting?
      }
    })
  end,
}
