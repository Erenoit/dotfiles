vim.filetype.add({
  pattern = {
    [".*.slint"] = "slint",
    [".*.wgsl"] = "wgsl",
  },
})
