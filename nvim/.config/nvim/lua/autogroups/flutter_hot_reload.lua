vim.api.nvim_create_augroup("flutter_hot_reload", { clear = true })

vim.api.nvim_create_autocmd(
  "BufWritePost",
  { group = "flutter_hot_reload",
    pattern = "*.dart",
    callback = function()
      vim.fn.system("tmux send-keys -t 1 r")
    end
  })
