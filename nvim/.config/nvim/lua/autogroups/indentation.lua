vim.api.nvim_create_augroup("indentation", { clear = true })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "go",
    command = "setlocal expandtab tabstop=4 shiftwidth=4 softtabstop=4"
  })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "python",
    command = "setlocal expandtab tabstop=4 shiftwidth=4 softtabstop=4"
  })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "dart",
    command = "setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2"
  })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "vim",
    command = "setlocal expandtab tabstop=4 shiftwidth=4 softtabstop=4"
  })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "html",
    command = "setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2"
  })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "htmldjango",
    command = "setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2"
  })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "css",
    command = "setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2"
  })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "scss",
    command = "setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2"
  })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "javascript",
    command = "setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2"
  })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "typescript",
    command = "setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2"
  })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "javascriptreact",
    command = "setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2"
  })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "typescriptreact",
    command = "setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2"
  })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "svelte",
    command = "setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2"
  })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "handlebars",
    command = "setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2"
  })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "ember",
    command = "setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2"
  })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "json",
    command = "setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2"
  })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "yaml",
    command = "setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2"
  })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "xml",
    command = "setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2"
  })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "lua",
    command = "setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2"
  })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "fortran",
    command = "setlocal expandtab tabstop=4 shiftwidth=4 softtabstop=4"
  })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "rust",
    command = "setlocal expandtab tabstop=4 shiftwidth=4 softtabstop=4"
  })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "c",
    command = "setlocal expandtab tabstop=4 shiftwidth=4 softtabstop=4"
  })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "cpp",
    command = "setlocal expandtab tabstop=4 shiftwidth=4 softtabstop=4"
  })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "make",
    command = "setlocal noexpandtab tabstop=4 shiftwidth=4 softtabstop=4"
  })

vim.api.nvim_create_autocmd(
  "Filetype",
  { group = "indentation",
    pattern = "markdown",
    command = [[setlocal expandtab tabstop=4 shiftwidth=4 softtabstop=4
                setlocal spell spelllang=en_us
                setlocal wrap linebreak nolist
                set textwidth=100]]
  })
