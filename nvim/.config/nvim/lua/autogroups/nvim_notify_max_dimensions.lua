vim.api.nvim_create_augroup("update_noftify_width", { clear = true })

vim.api.nvim_create_autocmd(
  "VimResized",
  { group = "update_noftify_width",
    pattern = "*",
    callback = function()
      require("notify").instance({
        max_height = math.max(vim.api.nvim_win_get_height(0) / 2, 10),
        max_width = math.max(vim.api.nvim_win_get_width(0) / 2, 30),
      }, true)
    end
  })
