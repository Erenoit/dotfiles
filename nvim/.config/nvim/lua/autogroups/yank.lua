vim.api.nvim_create_augroup("yank", { clear = true })

vim.api.nvim_create_autocmd(
  "TextYankPost",
  { group = "yank",
    pattern = "*",
    callback = function() vim.highlight.on_yank{ higroup = "IncSearch", timeout = 50 } end,
--    silent = true
  })

