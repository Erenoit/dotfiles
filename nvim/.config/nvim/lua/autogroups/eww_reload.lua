vim.api.nvim_create_augroup("eww_reload", { clear = true })

vim.api.nvim_create_autocmd(
  "BufWritePost",
  { group = "eww_reload",
    pattern = "*.yuck",
    callback = function()
      vim.fn.system("eww reload")
      vim.fn.system("setbar")
    end
  })
