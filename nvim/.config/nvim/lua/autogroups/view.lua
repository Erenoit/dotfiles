vim.api.nvim_create_augroup("view", { clear = true })

vim.api.nvim_create_autocmd(
  "BufWinLeave",
  { group = "view",
    pattern = "?*",
    command = "mkview"
  })

vim.api.nvim_create_autocmd(
  "BufWinEnter",
  { group = "view",
    pattern = "?*",
    command = "loadview",
--    silent = true
  })

