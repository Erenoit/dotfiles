-- vim:fileencoding=utf-8:foldmethod=marker

-- IMPORTS {{{

  -- Base
import XMonad
import System.Directory
import System.IO (hPutStrLn)
import System.Exit (exitSuccess)
import qualified XMonad.StackSet as W

    -- Actions
import XMonad.Actions.CopyWindow (kill1)
import XMonad.Actions.CycleWS (Direction1D(..), moveTo, shiftTo, WSType(..), nextScreen, prevScreen)
import XMonad.Actions.MouseResize
import XMonad.Actions.Promote
import XMonad.Actions.RotSlaves (rotSlavesDown, rotAllDown)
import XMonad.Actions.WindowGo (runOrRaise)
import XMonad.Actions.WithAll (sinkAll, killAll)
import qualified XMonad.Actions.Search as S

    -- Data
import Data.Char (isSpace, toUpper)
import Data.Maybe (fromJust)
import Data.Monoid
import Data.Maybe (isJust)
import Data.Tree
import qualified Data.Map as M

    -- Hooks
import XMonad.Hooks.DynamicLog (dynamicLogWithPP, wrap, xmobarPP, xmobarColor, shorten, PP(..))
import XMonad.Hooks.EwmhDesktops  -- for some fullscreen events, also for xcomposite in obs.
import XMonad.Hooks.ManageDocks (avoidStruts, docksEventHook, manageDocks, ToggleStruts(..))
import XMonad.Hooks.ManageHelpers (isFullscreen, doFullFloat, doCenterFloat)
import XMonad.Hooks.ServerMode
import XMonad.Hooks.SetWMName
import XMonad.Hooks.WorkspaceHistory

    -- Layouts
import XMonad.Layout.Accordion
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.ResizableTile
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns

    -- Layouts modifiers
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.Magnifier
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import XMonad.Layout.WindowNavigation
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))

   -- Utilities
import XMonad.Util.Dmenu
import XMonad.Util.EZConfig (additionalKeysP)
import XMonad.Util.NamedScratchpad
import XMonad.Util.Run (runProcessWithInput, safeSpawn, spawnPipe)
import XMonad.Util.SpawnOnce
-- }}}

-- FONT {{{

myFont = "mononoki Nerd Font:regular:size=10:antialias=true:hinting=true"
-- }}}

-- DEFAULT PROGRAMS {{{

myModMask  = mod1Mask -- for Super key -> mod4Mask

myTerm = "alacritty"
myBrowser  = "firefox"
myEditor   = myTerm ++ " -e vim" 
-- }}}

-- COLORS {{{

-- HackTheBox theme
-- https://github.com/audibleblink/hackthebox.vim
hackthebox = [
        ("background",   "#1a2332"),
        ("foreground",   "#a4b1cd"),

        ("background_l", "#313f55"),
        ("foreground_l", "#ffffff"),

        ("black",        "#000000"),
        ("red",          "#ff3e3e"),
        ("green",        "#9fef00"),
        ("yellow",       "#ffaf00"),
        ("blue",         "#004cff"),
        ("magenta",      "#9f00ff"),
        ("cyan",         "#2ee7b6"),
        ("white",        "#ffffff"),

        ("black_l",      "#666666"),
        ("red_l",        "#ff8484"),
        ("green_l",      "#c5f467"),
        ("yellow_l",     "#ffcc5c"),
        ("blue_l",       "#5cb2ff"),
        ("magenta_l",    "#c16cfa"),
        ("cyan_l",       "#5cecc6"),
        ("white_l",      "#ffffff")
    ]

-- Tokyo Night Theme
-- https://github.com/folke/tokyonight.nvim
tokyonight = [
        ("background",   "#1a1b26"),
        ("foreground",   "#c0caf5"),

        ("background_l", "#30313b"),
        ("foreground_l", "#c6cff6"),

        ("black",        "#15161e"),
        ("red",          "#f7768e"),
        ("green",        "#9ece66"),
        ("yellow",       "#e0af68"),
        ("blue",         "#004cff"),
        ("magenta",      "#bb9af7"),
        ("cyan",         "#7dcfff"),
        ("white",        "#a9b1d6"),

        ("black_l",      "#414868"),
        ("red_l",        "#f78399"),
        ("green_l",      "#a7d278"),
        ("yellow_l",     "#e3b777"),
        ("blue_l",       "#87abf7"),
        ("magenta_l",    "#c1a4f7"),
        ("cyan_l",       "#8ad3ff"),
        ("white_l",      "#c0caf5")
    ]

myColors :: [(String, String)]
myColors = tokyonight
-- }}}

-- WORKSPACES {{{

--myWorkspaces = ["","","","ﭮ","","","","","",""]
myWorkspaces = ["1","2","3","4","5","6","7","8","9"]
myWorkspaceIndices = M.fromList $ zipWith (,) myWorkspaces [1..] -- (,) == \x y -> (x,y)

clickable ws = "<action=xdotool key super+"++show i++">"++ws++"</action>"
    where i = fromJust $ M.lookup ws myWorkspaceIndices
-- }}}

-- SCRATCHPADS {{{

myScratchPads :: [NamedScratchpad]
myScratchPads = [
        NS "ncmpcpp"  spawnNCMPC  findNCMPC  manageNCMPC,
        NS "todo"     spawnTODO   findTODO   manageTODO,
        NS "terminal" spawnTERM   findTERM   manageTERM,
        NS "ranger"   spawnRANGER findRANGER manageRANGER
    ]
    where
        spawnNCMPC   = myTerm ++ " --class dropdown_ncmpcpp"
        findNCMPC    = className =? "dropdown_ncmpcpp"
        manageNCMPC  = manageDEF

        spawnTODO    = "superproductivity"
        findTODO     = title =? "Super Productivity"
        manageTODO   = manageDEF

        spawnTERM    = myTerm ++ " --class dropdown_term"
        findTERM     = className =? "dropdown_term"
        manageTERM   = manageDEF

        spawnRANGER  = myTerm ++ " --class dropdown_ranger"
        findRANGER   = className =? "dropdown_ranger"
        manageRANGER = manageDEF

        manageDEF = customFloating $ W.RationalRect l t w h
            where
                h = 0.9
                w = 0.9
                t = 0.95 -h
                l = 0.95 -w
-- }}}

-- LAYOUTS {{{

-- BORDERS
myBorderWidth = 2

myNormalBorderColor  = "#5cb2ff"
myFocusedBorderColor = "#004cff"

-- MARGIN
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

-- Layout Options
tall       = renamed [Replace "tall"]
             $ smartBorders
             $ windowNavigation
             -- $ addTabs shrinkText myTabTheme
             $ subLayout [] (smartBorders Simplest)
             $ limitWindows 12
             $ mySpacing 8
             $ ResizableTall 1 (3/100) (1/2) []

floats     = renamed [Replace "floats"]
             $ smartBorders
             $ limitWindows 20 simplestFloat

grid       = renamed [Replace "grid"]
             $ smartBorders
             $ windowNavigation
             -- $ addTabs shrinkText myTabTheme
             $ subLayout [] (smartBorders Simplest)
             $ limitWindows 12
             $ mySpacing 8
             $ mkToggle (single MIRROR)
             $ Grid (16/10)

full       = renamed [Replace "full"]
             -- $ smartBorders
             -- $ windowNavigation
             -- $ addTabs shrinkText myTabTheme
             -- $ subLayout [] (smartBorders Simplest)
             $ limitWindows 12
             $ mySpacing 8
             $ Full

mirrorTall = renamed [Replace "mirror tall"]
             -- $ smartBorders
             -- $ windowNavigation
             -- $ addTabs shrinkText myTabTheme
             -- $ subLayout [] (smartBorders Simplest)
             $ limitWindows 12
             $ mySpacing 8
             $ Mirror (ResizableTall 1 (3/100) (1/2) [])
             

myLayouts = avoidStruts $ mouseResize $ windowArrange $ T.toggleLayouts floats
               $ mkToggle (NBFULL ?? NOBORDERS ?? EOT) myDefaultLayout
             where
               myDefaultLayout =      withBorder myBorderWidth tall 
                                  ||| floats
                                  ||| grid
--                                  ||| full
--                                  ||| mirrorTall


myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
    { swn_font              = "xft:Ubuntu:bold:size=60"
    , swn_fade              = 1.0
    , swn_bgcolor           = "#1c1f24"
    , swn_color             = "#ffffff"
    }

-- }}}

-- KEYS {{{

myKeys :: [(String, X ())]
myKeys = [
        ("M-C-r", spawn "xmonad --recompile")       -- Recompiles xmonad
    ]
-- }}}

-- MOUSE {{{

myFocusFollowsMouse = True
myClickJustFocuses  = False
-- }}}

-- HOOKS {{{

myStartupHook :: X ()
myStartupHook = do
    setWMName "LG3D"

myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook = composeAll
     -- 'doFloat' forces a window to float.  Useful for dialog boxes and such.
     -- using 'doShift ( myWorkspaces !! 7)' sends program to workspace 8!
     -- I'm doing it this way because otherwise I would have to write out the full
     -- name of my workspaces and the names would be very long if using clickable workspaces.
     [ className =? "confirm"         --> doFloat
     , className =? "file_progress"   --> doFloat
     , className =? "dialog"          --> doFloat
     , className =? "download"        --> doFloat
     , className =? "error"           --> doFloat
     , className =? "Gimp"            --> doFloat
     , className =? "notification"    --> doFloat
     , className =? "pinentry-gtk-2"  --> doFloat
     , className =? "splash"          --> doFloat
     , className =? "toolbar"         --> doFloat
     , className =? "Yad"             --> doCenterFloat
     , title =? "Oracle VM VirtualBox Manager"  --> doFloat
     , title =? "Mozilla Firefox"     --> doShift ( myWorkspaces !! 1 )
     , className =? "Brave-browser"   --> doShift ( myWorkspaces !! 1 )
     , className =? "mpv"             --> doShift ( myWorkspaces !! 7 )
     , className =? "Gimp"            --> doShift ( myWorkspaces !! 8 )
     , className =? "VirtualBox Manager" --> doShift  ( myWorkspaces !! 4 )
     , (className =? "firefox" <&&> resource =? "Dialog") --> doFloat  -- Float Firefox Dialog
     , isFullscreen -->  doFullFloat
     ] <+> namedScratchpadManageHook myScratchPads

-- }}}

-- MAIN {{{

windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset :: X (Maybe String)

main:: IO()
main = do
    -- Launch xmobar (Up to 3 screens)
    xmproc0 <- spawnPipe ("xmobar -x 0 $HOME/.config/xmobar/xmobarrc")
    xmproc1 <- spawnPipe ("xmobar -x 1 $HOME/.config/xmobar/xmobarrc")
    xmproc2 <- spawnPipe ("xmobar -x 2 $HOME/.config/xmobar/xmobarrc")

    -- Set Xmonad
    xmonad $ ewmh def
        {
            modMask = myModMask,
            terminal = myTerm,
            workspaces = myWorkspaces,

            borderWidth = myBorderWidth,
            normalBorderColor = myNormalBorderColor,
            focusedBorderColor = myFocusedBorderColor,
            
            manageHook = myManageHook <+> manageDocks,
            --layoutHook = showWName myShowWNameTheme $ myLayouts,
            startupHook = myStartupHook,
            handleEventHook = docksEventHook <+> fullscreenEventHook,
            logHook = dynamicLogWithPP $ namedScratchpadFilterOutWorkspacePP $ xmobarPP
            -- Xmobar Settings
            {
                ppOutput = \x -> hPutStrLn xmproc0 x   -- xmobar on monitor 1
                              >> hPutStrLn xmproc1 x   -- xmobar on monitor 2
                              >> hPutStrLn xmproc2 x   -- xmobar on monitor 3
                -- Current workspace
              , ppCurrent = xmobarColor "#a4b1cd" "#ff3e3e"
                --    . wrap ("<box type=Bottom width=2 mb=2 color=" ++ color06 ++ ">") "</box>"
                -- Visible but not current workspace
              , ppVisible = xmobarColor "#a4b1cd" "#313f55" . clickable
                -- Hidden workspace
              , ppHidden = xmobarColor "#a4b1cd" "" . clickable
                --    . wrap ("<box type=Top width=2 mt=2 color=" ++ color05 ++ ">") "</box>"
                -- Hidden workspaces (no windows)
              , ppHiddenNoWindows = xmobarColor "#313f55" ""  . clickable
                -- Title of active window
              , ppTitle = xmobarColor "#a4b1cd" "#313f55" . shorten 60
                -- Separator character
              , ppSep =  "<fc=" ++ "#ff3e3e" ++ "> <fn=1>|</fn> </fc>"
                -- Urgent workspace
              , ppUrgent = xmobarColor "#ffaf00" "" . wrap "!" "!"
                -- Adding # of windows on current workspace to the bar
              , ppExtras  = [windowCount]
                -- order of things in xmobar
              , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
            }
        }
-- }}}

