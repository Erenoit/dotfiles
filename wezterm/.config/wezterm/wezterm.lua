local config = {}

config.font = require("wezterm").font_with_fallback({
  "Iosevka Nerd Font",
  "Sarasa Term J",
  "JoyPixels"
})
config.font_size = 12

config.color_scheme_dirs = { "~/.config/wezterm/colors" }
config.color_scheme = "tokyonight"
config.colors = { background = "#000000" }
config.enable_tab_bar = false
config.window_background_opacity = 0.6

config.use_ime = true
config.use_dead_keys = false











return config
