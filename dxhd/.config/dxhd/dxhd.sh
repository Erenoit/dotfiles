#!/bin/sh
## vim:fileencoding=utf-8:foldmethod=marker

##### Programs {{{

# super + Return
st

# super + shift + Return
wezterm

# super + w
firefox

# super + shift + w
qutebrowser

# super + ctrl + w
torbrowser-launcher

# super + r
rofi -show run

# super + e
thunar

# super + m
mailspring

# super + s
spotify

# print
flameshot gui

# {ctrl, shift} + print
flameshot {full, screen}

# super + ctrl + r
dxhd -r

# super + o
slock
## }}}

##### Rofi Scripts {{{

# super + ctrl + q
power_menu

# super+ shift + b
bookshelf_menu

# super + shift + e
emoji_selector
## }}}

##### F Keys {{{

# XF86KbdBrightness { Up, Down }
id=8
brightnessctl --device="asus::kbd_backlight" s { +1, 1- }
b_percentage=$(brightnessctl --device="asus::kbd_backlight" g)*100/$(brightnessctl --device="asus::kbd_backlight" m)
notify-send "Keyboard Brightness" -r $id -h int:value:$((b_percentage)) -u low

# XF86MonBrightness { Up, Down }
id=9
one_percent=$(($(brightnessctl --device="intel_backlight" m)/100))
brightnessctl --device="intel_backlight" s { +$one_percent, $one_percent- }
b_percentage=$(($(brightnessctl --device="intel_backlight" g)/$one_percent))
notify-send "Screen Brightness" -r $id -h int:value:$((b_percentage)) -u low

# XF86DisplayOff
xset dpms force off

# XF86TouchpadToggle
id=10
device="$(xinput list | grep -P '(?<= )[\w\s:]*(?i)(touchpad|synaptics)(?-i).*?(?=\s*id)' -o | head -n1)"

[[ "$(xinput list-props "$device" | grep -P ".*Device Enabled.*\K.(?=$)" -o)" == "1" ]] &&
    (xinput disable "$device" && notify-send "Touchpad is off" -r $id -u low) ||
    (xinput enable "$device"  && notify-send "Touchpad is on"  -r $id -u low) 

# XF86AudioMute
pamixer -t

# XF86Audio { Lower, Raise } Volume
pamixer -{d,i} 1
pkill -RTMIN+3 dwmblocks

# XF86Audio {Stop, Prev, Play, Pause, Next}
playerctl {stop, previous, play-pause, play-pause, next}
## }}}

